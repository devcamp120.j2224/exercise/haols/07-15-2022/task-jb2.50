import com.devcamp.S50.jbr250.Account;
import com.devcamp.S50.jbr250.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(5, "Khoa", 25);
        Customer customer2 = new Customer(7, "Nga", 15);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        Account account1 = new Account(9, customer1);
        Account account2 = new Account(3, customer2, 1888);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

    }
}
